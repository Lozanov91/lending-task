<?php

use Lending\Models\Loan;

use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    const DATE_FORMAT = 'd/m/Y';
    private $loan;

    public function setUp() : void
    {
        $this->loan = new Loan(DateTime::createFromFormat(self::DATE_FORMAT, '01/10/2015'), DateTime::createFromFormat(self::DATE_FORMAT, '15/11/2015'));
    }

    public function testLoanIsSetCorrectly()
    {
        $this->assertObjectHasAttribute('startDate', $this->loan);
        $this->assertObjectHasAttribute('endDate', $this->loan);
        $this->assertInstanceOf(DateTime::class, $this->loan->getEndDate());
    }

    public function testLoanGetEndDate()
    {
        $endDate = $this->loan->getEndDate();
        $this->assertInstanceOf(DateTime::class, $endDate);
    }

    public function testLoanIsOpenForInvestment()
    {
        $actual = $this->loan->validateIsOpenForInvestment(DateTime::createFromFormat(self::DATE_FORMAT, '10/10/2015'));
        $this->assertEquals(true, $actual);
    }

    public function testLoanIsNotOpenForInvestment()
    {
        $actual = $this->loan->validateIsOpenForInvestment(DateTime::createFromFormat(self::DATE_FORMAT, '01/01/2015'));
        $this->assertEquals(false, $actual);
    }
}