<?php

use Lending\Models\Investor;
use Lending\Models\Tranche;
use Lending\Models\Loan;

use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{
    private $investor;

    public function setUp() : void
    {
        $this->investor = new Investor('Investor 1', 1000);
    }

    public function testInvestorIsSetCorrectly() {
        $this->assertIsObject($this->investor);
        $this->assertObjectHasAttribute('name', $this->investor);
        $this->assertObjectHasAttribute('amount', $this->investor);
        $this->assertObjectHasAttribute('investments', $this->investor);
    }

    public function testInvestorGetName()
    {
        $this->assertIsString($this->investor->getName());
    }

    public function testInvestorCanInvest()
    {
        $investedAmount = 500;
        $investmentDate = DateTime::createFromFormat('d/m/Y', '03/10/2015');
        $calculationDate = DateTime::createFromFormat('d/m/Y', '04/10/2015');

        $tranche = $this->getMockBuilder(Tranche::class)
            ->disableOriginalConstructor()
            ->setMethods(["invest"])
            ->getMock();
        $tranche->expects($this->any())
            ->method('invest')
            ->will($this->returnSelf());

        $this->investor->invest($tranche, $investedAmount, $investmentDate);

        $this->assertEquals(0.00, $this->investor->calculateInterestForDate($calculationDate));
    }

    public function testInvestorInvestWillReturnException()
    {
        $investedAmount = 1001;
        $investmentDate = DateTime::createFromFormat('d/m/Y', '04/10/2015');

        $tranche = $this->getMockBuilder(Tranche::class)
            ->disableOriginalConstructor()
            ->setMethods(["invest"])
            ->getMock();
        $tranche->expects($this->any())
            ->method('invest')
            ->will($this->returnSelf());

        $this->expectException(RuntimeException::class);
        $this->investor->invest($tranche, $investedAmount, $investmentDate);
    }
}