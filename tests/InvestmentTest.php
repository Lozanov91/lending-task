<?php

use Lending\Models\Investment;

use Lending\Models\Investor;
use Lending\Models\Loan;
use Lending\Models\Tranche;
use PHPUnit\Framework\TestCase;

class InvestmentTest extends TestCase
{
    private $amount;
    private $date;
    private $investor;

    public function setUp() : void
    {
        $this->amount = 500;
        $this->date = DateTime::createFromFormat('d/m/Y', '03/10/2015');

        $investor = $this->getMockBuilder(Investor::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->investor = $investor;
    }

    public function testCalculateEarnings()
    {
        $loan = $this->getMockBuilder(Loan::class)
            ->disableOriginalConstructor()
            ->getMock();

        $loan->expects($this->any())
            ->method('getEndDate')
            ->willReturn(DateTime::createFromFormat('d/m/Y', '15/11/2015'));

        $tranche = $this->getMockBuilder(Tranche::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tranche->expects($this->any())
            ->method('getLoan')
            ->willReturn($loan);

        $tranche->expects($this->any())
            ->method('getInterest')
            ->willReturn(3);

        $investment = new Investment($this->amount, $this->date, $tranche, $this->investor);

        $actual = $investment->calculateEarning(DateTime::createFromFormat('d/m/Y', '01/11/2015'));

        $this->assertIsNumeric($actual);
        $this->assertGreaterThan(0, $actual);
    }

    public function testCalculateEarningsCheckForLoanEndDate()
    {
        $loan = $this->getMockBuilder(Loan::class)
            ->disableOriginalConstructor()
            ->getMock();

        $loan->expects($this->any())
            ->method('getEndDate')
            ->willReturn(DateTime::createFromFormat('d/m/Y', '01/01/2015'));

        $tranche = $this->getMockBuilder(Tranche::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tranche->expects($this->any())
            ->method('getLoan')
            ->willReturn($loan);

        $tranche->expects($this->any())
            ->method('getInterest')
            ->willReturn(3);

        $investment = new Investment($this->amount, $this->date, $tranche, $this->investor);

        // check for month 09
        // investment made in month 10
        $actual = $investment->calculateEarning(DateTime::createFromFormat('d/m/Y', '01/10/2015'));

        $this->assertIsNumeric($actual);
        $this->assertEquals(0, $actual);
    }

    public function testCalculateEarningsWillReturnZero()
    {
        $loan = $this->getMockBuilder(Loan::class)
            ->disableOriginalConstructor()
            ->getMock();

        $loan->expects($this->any())
            ->method('getEndDate')
            ->willReturn(DateTime::createFromFormat('d/m/Y', '25/10/2015'));

        $tranche = $this->getMockBuilder(Tranche::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tranche->expects($this->any())
            ->method('getLoan')
            ->willReturn($loan);

        $tranche->expects($this->any())
            ->method('getInterest')
            ->willReturn(3);

        $investment = new Investment($this->amount, $this->date, $tranche, $this->investor);

        $actual = $investment->calculateEarning(DateTime::createFromFormat('d/m/Y', '01/11/2015'));

        $this->assertIsNumeric($actual);
        $this->assertGreaterThan(0, $actual);
    }
}