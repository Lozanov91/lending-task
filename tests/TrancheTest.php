<?php

use Lending\Models\Tranche;
use Lending\Models\Loan;

use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    private $dateFormat;
    private $tranche;
    private $loan;

    public function setUp() : void
    {
        $this->dateFormat = 'd/m/Y';
        $this->loan = new Loan(DateTime::createFromFormat($this->dateFormat, '01/10/2015'), DateTime::createFromFormat($this->dateFormat, '15/11/2015'));
        $this->tranche = new Tranche($this->loan, 'Tranche A', 3, 1000);
    }

    public function testTrancheIsSetCorrectly()
    {
        $this->assertIsObject($this->tranche);
        $this->assertObjectHasAttribute('name', $this->tranche);
        $this->assertObjectHasAttribute('loan', $this->tranche);
        $this->assertObjectHasAttribute('interest', $this->tranche);
        $this->assertObjectHasAttribute('maximumAmount', $this->tranche);
        $this->assertObjectHasAttribute('investedAmount', $this->tranche);
    }

    public function testCanInvest()
    {
        $amountToInvest = 1000;
        $investmentDate = DateTime::createFromFormat($this->dateFormat, '03/10/2015');

        $actual = $this->tranche->invest($amountToInvest, $investmentDate);

        $this->assertEquals(null , $actual);
    }

    public function testExceedMaximumAmountToInvest()
    {
        $amountToInvest = 1000;
        $investmentDate = DateTime::createFromFormat($this->dateFormat, '03/10/2015');

        $this->tranche->invest($amountToInvest, $investmentDate);

        $this->expectException(RuntimeException::class);
        $this->tranche->invest(1, $investmentDate);
    }

    public function testTrancheThrowsExceptionIfLoanIsNotOpen()
    {
        $amountToInvest = 1000;
        $investmentDate = DateTime::createFromFormat($this->dateFormat, '03/12/2015');

        $this->expectException(OutOfRangeException::class);
        $this->tranche->invest($amountToInvest, $investmentDate);
    }

    public function testGetInterest()
    {
        $actual = $this->tranche->getInterest();

        $this->assertEquals(3, $actual);
    }

    public function testGetLoan()
    {
        $loan = $this->tranche->getLoan();

        $this->assertInstanceOf(Loan::class, $loan);
    }
}