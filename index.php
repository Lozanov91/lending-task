<?php

use Lending\Models\Loan;

use Lending\Models\Investor;
use Lending\Models\Tranche;

error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';

$dateFormat = 'd/m/Y';
$calculationDate = DateTime::createFromFormat($dateFormat, '01/11/2015');

$loan = new Loan(DateTime::createFromFormat($dateFormat, '01/10/2015'), DateTime::createFromFormat($dateFormat, '15/11/2015'));

$trancheA = new Tranche($loan, 'Tranche A', 3, 1000);
$trancheB = new Tranche($loan, 'Tranche B', 6, 1000);

$investor1 = new Investor('Investor 1', 1000);
$investor2 = new Investor('Investor 2', 1000);
$investor3 = new Investor('Investor 3', 1000);
$investor4 = new Investor('Investor 4', 1000);

try {
    $investor1->invest($trancheA, 1000, DateTime::createFromFormat($dateFormat, '03/10/2015'));
    echo $investor1->getName().' earns '.$investor1->calculateInterestForDate($calculationDate).' pounds'.PHP_EOL;
} catch (Exception $e) {
    echo $investor1->getName().' '.$e->getMessage().PHP_EOL;
}

try {
    $investor2->invest($trancheA, 1, DateTime::createFromFormat($dateFormat, '04/10/2015'));
    echo $investor2->getName().' earns '.$investor2->calculateInterestForDate($calculationDate).' pounds'.PHP_EOL;
} catch (Exception $e) {
    echo $investor2->getName().' '.$e->getMessage().PHP_EOL;
}

try {
    $investor3->invest($trancheB, 500, DateTime::createFromFormat($dateFormat, '10/10/2015'));
    echo $investor3->getName().' earns '.$investor3->calculateInterestForDate($calculationDate).' pounds'.PHP_EOL;
} catch (Exception $e) {
    echo $investor3->getName().' '.$e->getMessage().PHP_EOL;
}

try {
    $investor4->invest($trancheB, 1100, DateTime::createFromFormat($dateFormat, '25/10/2015'));
    echo $investor4->getName().' earns '.$investor4->calculateInterestForDate($calculationDate).' pounds'.PHP_EOL;
} catch (Exception $e) {
    echo $investor4->getName().' '.$e->getMessage().PHP_EOL;
}
