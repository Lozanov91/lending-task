<?php

namespace Lending\Models;

use DateTime;

class Investor
{
    private $name;
    private $amount = 0;
    private $investments = [];

    /**
     * Investor constructor.
     * @param string $name
     * @param int $amount
     */
    public function __construct(string $name, int $amount)
    {
        $this->name = $name;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param Tranche $tranche
     * @param int $amount
     * @param DateTime $date
     */
    public function invest(Tranche $tranche, int $amount, DateTime $date) : void
    {
        $this->removeFundsFromWallet($amount);
        $tranche->invest($amount, $date);

        $this->investments[] = new Investment($amount, $date, $tranche, $this);
    }

    /**
     * @param int $amount
     */
    private function removeFundsFromWallet(int $amount) : void
    {
        if ($this->amount < $amount) {
            throw  new \RuntimeException('Don\'t have enough funds in wallet.');
        }

        $this->amount -= $amount;
    }

    /**
     * @param DateTime $calculationDate
     * @return float
     */
    public function calculateInterestForDate(DateTime $calculationDate) : float
    {
        $earnings = 0.00;

        foreach ($this->investments as $investment) {
            $earnings += $investment->calculateEarning($calculationDate);
        }

        return $earnings;
    }
}