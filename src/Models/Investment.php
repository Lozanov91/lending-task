<?php

namespace Lending\Models;

use DateTime;

class Investment
{
    private $amount;
    private $date;
    private $tranche;
    private $investor;

    /**
     * Investment constructor.
     * @param int $amount
     * @param DateTime $date
     * @param Tranche $tranche
     * @param Investor $investor
     */
    public function __construct(int $amount, DateTime $date, Tranche $tranche, Investor $investor)
    {
        $this->amount = $amount;
        $this->date = $date;
        $this->tranche = $tranche;
        $this->investor = $investor;
    }

    /**
     * @param DateTime $calculationDate
     * @return float
     */
    public function calculateEarning(DateTime $calculationDate) : float
    {
        if ($this->checkInvestmentIsInRange($calculationDate)) {
            $daysForInterest = $this->getDaysForInterval($this->date, $this->tranche->getLoan());
            $monthlyInterest = ($this->tranche->getInterest() / 100) * $this->amount;

            $earning = ($monthlyInterest / (int)$this->date->format('t')) * $daysForInterest;

            return (float)number_format($earning, 2);
        }

        return 0.00;
    }

    /**
     * @param DateTime $calculationDate
     * @return bool
     */
    private function checkInvestmentIsInRange(DateTime $calculationDate) : bool
    {
        return ($calculationDate->format('Y-m') > $this->date->format('Y-m'));
    }

    /**
     * @param DateTime $investmentDate
     * @param Loan $loan
     * @return int
     */
    private function getDaysForInterval(DateTime $investmentDate, Loan $loan) : int
    {
        $endDateOfInvestment = DateTime::createFromFormat('Y-m-d', $investmentDate->format('Y-m-t'));

        // check if loan ends before last day of investment month
        if ($loan->getEndDate() < $endDateOfInvestment) {
            $endDateOfInvestment = $loan->getEndDate();
        }

        return (int)$endDateOfInvestment->diff($investmentDate)->days + 1;
    }
}