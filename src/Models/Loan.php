<?php

namespace Lending\Models;

use DateTime;

class Loan
{
    private $startDate;
    private $endDate;

    /**
     * Loan constructor.
     * @param DateTime $startDate
     * @param DateTime $endDate
     */
    public function __construct(DateTime $startDate, DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate() : DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $investmentDate
     * @return bool
     */
    public function validateIsOpenForInvestment(DateTime $investmentDate) : bool
    {
        return ($investmentDate > $this->startDate && $investmentDate < $this->endDate);
    }
}
