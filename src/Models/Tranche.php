<?php

namespace Lending\Models;

use DateTime;

use OutOfRangeException;
use RuntimeException;

class Tranche
{
    private $loan;
    private $name;
    private $interest;
    private $maximumAmount;
    private $investedAmount = 0;

    /**
     * Tranche constructor.
     * @param Loan $loan
     * @param string $name
     * @param int $interest
     * @param int $maximumAmount
     */
    public function __construct(Loan $loan, string $name, int $interest, int $maximumAmount)
    {
        $this->loan = $loan;
        $this->name = $name;
        $this->interest = $interest;
        $this->maximumAmount = $maximumAmount;
    }

    /**
     * @param int $amountToInvest
     * @param DateTime $investmentDate
     */
    public function invest(int $amountToInvest, DateTime $investmentDate) : void
    {
        if (!$this->loan->validateIsOpenForInvestment($investmentDate)) {
            throw new OutOfRangeException('The loan was closed for investment.');
        }

        if (($this->investedAmount + $amountToInvest) > $this->maximumAmount) {
            throw new RuntimeException('exceeds the maximum amount.');
        }

        $this->investedAmount += $amountToInvest;
    }

    /**
     * @return int
     */
    public function getInterest() : int
    {
        return $this->interest;
    }

    /**
     * @return Loan
     */
    public function getLoan() : Loan
    {
        return $this->loan;
    }
}